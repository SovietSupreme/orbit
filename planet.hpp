#ifndef _PLANET_HPP_
#define _PLANET_HPP_
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <string>

#include "vector.hpp"
#include "renderer.hpp"

class Planet {
    private:
        double k = -3.5000;
        double resolution = j_abs(k) * j_abs(3000);
    public:
        double n = 0;
	    j_vector2d *pos;
	    int r = 0;
	    double speed = 0.00;
	    double angle = 0.00;
        Planet *parent = NULL;
        Planet *child = NULL;
        j_color color = Colors[jrandom(49)];
    
    public:
        //
        // used by addChild() mFunction
        //
        Planet(int x_, int y_, int n_, int r_, Planet *p_) {
            parent = p_;
            pos = new j_vector2d(x_,y_);
            n = (parent->n + jrandom(1,2));
            r = r_;
            speed = (pow(k,n) / resolution * (DEG_TO_RAD));
		    angle = -J_PI/2;

        }
        Planet(int x_, int y_, int n_, int r_) {
            pos = new j_vector2d(x_,y_);
            n = n_;
            r = r_;
            speed = (pow(k,n)) / resolution * (DEG_TO_RAD);
		    angle = -J_PI/2;
        }
        ~Planet() {
            delete(pos);
            printf("object destroyed!!\n");
        }
        void addChild();
	    void update();
	    void show();
        int numParents();
        int numChildren();
};

class Sphere {
    private:
        int kFactor = -3;
        double nFactor = 4;
        double precision = 50;
	    j_vector2d *pos;
	    double r = 0.000;
	    double speed = 0.00;
	    double angle = 0.00;
        j_color color = Colors[J_Cyan];
    public:
        Sphere(double x_ = 0.000, double y_ = 0.000) {
            pos = new j_vector2d(x_,y_);
        }
        ~Sphere() {
            delete(pos);
            printf("Sphere destroyed!!\n");
        }

        inline void setKfactor(int k_) { kFactor = k_; }
        inline void setNfactor(double n_) { nFactor = n_; }
        inline void setPrecision( double p_) { precision = p_;}
        inline void setPosX(double x_) { pos->x = x_; }
        inline void setPosY(double y_) { pos->y = y_; }
        inline void setRadius(double r_) { r = r_; }
        inline void setAngle(double a_) { angle = a_; }
        inline void setColor(j_color c_) { color = c_; }
        inline void setSpeed() { speed = (pow(kFactor,nFactor) / precision ) * (DEG_TO_RAD); }

        inline int getKfactor() { return kFactor; }
        inline double getNfactor() { return nFactor; }
        inline double getPrecision() { return precision;}
        inline double getPosX() { return pos->x; }
        inline double getPosY() { return pos->y; }
        inline double getRadius() { return r; }
        inline double getSpeed() { return speed; }
        inline double getAngle() { return angle; }
        inline j_color getColor() { return color; }
        inline void draw() { pset(pos->x,pos->y,color); }


};

class Star {
    public:
        double x;
        double y;
        double z;
        j_color c;

    Star(double x_, double y_) {
        x = x_;
        y = y_;
        z = jrandom(1,5); 
    }

    ~Star() { }
};

#endif