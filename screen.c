#include <SDL.h>
#include <SDL_image.h>
#include <stdlib.h>
#include "screen.h"
#include "vector.hpp"

int w; //width of the screen
int h; //height of the screen

int mouseX;
int mouseY;

SDL_Event event = {0};
SDL_Window* window; // a single SDL window used
SDL_Surface* window_surface;
SDL_Surface* black_surface;
SDL_Renderer* renderer; // added for 2.0
const uint8_t* inkeys;

void createScreen(int width, int height, bool fullscreen)
{

 // glewExperimental = true;

 // if (glewInit() != GLEW_OK) {
 //   printf("Failed to initialize GLEW\n");
 //   return -1;
//}

  //SDL_GL_SetAttribute(GL_MAJOR_VERSION,3);
  //SDL_GL_SetAttribute(GL_MINOR_VERSION,3);

  int colorDepth = 32;
  w = width;
  h = height;

  if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
  {
    printf("Unable to init SDL: %s\n", SDL_GetError());
    SDL_Quit();
    exit(-1);
  }

  if(fullscreen)
  {
        window = SDL_CreateWindow("SDL2",
                          SDL_WINDOWPOS_UNDEFINED,
                          SDL_WINDOWPOS_UNDEFINED,
                          width, height,
                          SDL_WINDOW_FULLSCREEN_DESKTOP);

        //renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
        //SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "best");
        //SDL_RenderSetLogicalSize(renderer, width, height);
       //SDL_SaveBMP_RW()
  }
  else
  {
     window = SDL_CreateWindow("SDL2",
                          SDL_WINDOWPOS_CENTERED,
                          SDL_WINDOWPOS_CENTERED,
                          width, height,
                          0);

      

      //renderer = SDL_CreateRenderer(window,-1, SDL_RENDERER_ACCELERATED);
      //SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "best");
  }
  if (window == NULL)
  {
    printf("Unable to set video: %s\n", SDL_GetError());
    SDL_Quit();
    exit(-1);
  }
  //SDL_GLContext* gl_context = SDL_GL_CreateContext(window);
  window_surface = SDL_GetWindowSurface(window);

}

//Returns 1 if you close the window or press the escape key. Also handles everything that's needed per frame.
//Never put key input code right before done() or SDL may see the key as SDL_QUIT
bool done(bool quit_if_esc, bool delay) //delay makes CPU have some free time, use once per frame to avoid 100% usage of a CPU core
{
  if(delay) SDL_Delay(5); //so it consumes less processing power
  
  bool doneFlag = false;

  if(!SDL_PollEvent(&event)) {
    return doneFlag;
  }
  
  readKeys();
  
  SDL_GetMouseState(&mouseX,&mouseY);

  if(quit_if_esc && inkeys[SDL_SCANCODE_ESCAPE]) {
    doneFlag = true;
  }

  if(inkeys[SDL_SCANCODE_SPACE]) {
    cls(Colors[J_Black]);
  }
  
  if(event.type == SDL_QUIT) { 
    doneFlag = true;
  }
  return doneFlag;
}

//Updates the screen.  Has to be called to view new pixels, but use only after
//drawing the whole screen because it's slow.
void redraw()
{
  //SDL_BlitSurface(window_surface,NULL,win)
  SDL_UpdateWindowSurface(window);
  //SDL_RenderPresent(renderer);
  //SDL_GL_SwapWindow(window);
}

//Clears the screen to black
void cls(j_color color)
{
  //SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, 100);
  //SDL_RenderClear(renderer);
  SDL_FillRect( window_surface, NULL, SDL_MapRGB( window_surface->format, color.r, color.g, color.b ) );
}

//Gives value of pressed key to inkeys.
//the variable inkeys can then be used anywhere to check for input
//Normally you have to use readkeys every time you want to use inkeys, but the done() function also uses inkeys so it's not needed to use readkeys if you use done().
void readKeys()
{
  SDL_PollEvent(&event);
  inkeys = SDL_GetKeyboardState(NULL);
}

bool keyDown(int key) //this checks if the key is held down, returns true all the time until the key is up
{
  return inkeys[key];
}

bool keyPressed(int key) //this checks if the key is *just* pressed, returns true only once until the key is up again
{
 /*  if(keypressed.find(key) == keypressed.end()) keypressed[key] = 0;
  if(inkeys[key])
  {
    if(keypressed[key] == 0)
    {
      keypressed[key] = 1;
      return 1;
    }
  }
  else keypressed[key] = 0; */

  return false;
}

//Returns the time in milliseconds since the program started
uint32_t getTicks()
{
  return SDL_GetTicks();
}

double map(double value,double start1, double stop1, double start2, double stop2, bool withinBounds) {
  
  double newval = (value - start1) / (stop1 - start1) * (stop2 - start2) + start2;
  
  if(!withinBounds) {
    return newval;
  }
  if (start2 < stop2) {
    return constrain(newval, start2, stop2);
  } else {
    return constrain(newval, stop2, start2);
  }
}
/*
int map(int value,int start1, int stop1, int start2, int stop2, bool withinBounds) {

  int newval = (value - start1) / (stop1 - start1) * (stop2 - start2) + start2;
  
  if(!withinBounds) {
    return newval;
  }
  if (start2 < stop2) {
    return constrain(newval, start2, stop2);
  } else {
    return constrain(newval, stop2, start2);
  }
}
*/

double constrain(double value,double low, double high) {
    return MAX(MIN(value,high),low);
}


double distance(double x_, double y_, double x_2, double y_2) {
  return hypot(x_2 - x_, y_2 - y_);
}

int random(int min_, int max_) {

   return  min_ + ( std::rand() % ( max_ - min_ + 1 ) );
    
    
  //return map((double)max / rand(),0,max,0,1.0,true);
}
int random(int max_) {
   return  0 + ( std::rand() % ( max_ - 0 + 1 ) );
    
  //return map((double)max / rand(),0,max,0,1.0,true);
}

int j_abs(int a)
{
	return (a < 0) ? -a : a;
}

SDL_Surface* loadTexture( const char* path )
{
    //The final texture

    //Load image at specified path
    SDL_Surface* newSurface = IMG_Load( path );

    if( newSurface == NULL )
    {
      printf( "Unable to create surface from %s! SDL Error: %s\n", path, SDL_GetError() );
    }

    return newSurface;
}


//Fast vertical line from (x,y1) to (x,y2), with rgb color
bool verLine(int x, int y1, int y2, j_color color)
{
  if(y2 < y1) {y1 += y2; y2 = y1 - y2; y1 -= y2;} //swap y1 and y2
  if(y2 < 0 || y1 >= h  || x < 0 || x >= w) return 0; //no single point of the line is on screen
  if(y1 < 0) y1 = 0; //clip
  if(y2 >= w) y2 = h - 1; //clip

  Uint32 colorSDL = SDL_MapRGB(window_surface->format, color.r, color.g, color.b);
  Uint32* bufp;

  bufp = (Uint32*) window_surface->pixels + y1 * window_surface->pitch / 4 + x;
  for(int y = y1; y <= y2; y++)
  {
     *bufp = colorSDL;
     bufp += window_surface->pitch / 4;
  }
  return 1;
}

//Bresenham line from (x1,y1) to (x2,y2) with rgb color
bool drawLine(int x1, int y1, int x2, int y2, j_color color)
{
  if(x1 < 0 || x1 > w - 1 || x2 < 0 || x2 > w - 1 || y1 < 0 || y1 > h - 1 || y2 < 0 || y2 > h - 1) return 0;

  int deltax = j_abs(x2 - x1); //The difference between the x's
  int deltay = j_abs(y2 - y1); //The difference between the y's
  int x = x1; //Start x off at the first pixel
  int y = y1; //Start y off at the first pixel
  int xinc1, xinc2, yinc1, yinc2, den, num, numadd, numpixels, curpixel;

  if(x2 >= x1) //The x-values are increasing
  {
    xinc1 = 1;
    xinc2 = 1;
  }
  else //The x-values are decreasing
  {
    xinc1 = -1;
    xinc2 = -1;
  }
  if(y2 >= y1) //The y-values are increasing
  {
    yinc1 = 1;
    yinc2 = 1;
  }
  else //The y-values are decreasing
  {
    yinc1 = -1;
    yinc2 = -1;
  }
  if (deltax >= deltay) //There is at least one x-value for every y-value
  {
    xinc1 = 0; //Don't change the x when numerator >= denominator
    yinc2 = 0; //Don't change the y for every iteration
    den = deltax;
    num = deltax / 2;
    numadd = deltay;
    numpixels = deltax; //There are more x-values than y-values
  }
  else //There is at least one y-value for every x-value
  {
    xinc2 = 0; //Don't change the x for every iteration
    yinc1 = 0; //Don't change the y when numerator >= denominator
    den = deltay;
    num = deltay / 2;
    numadd = deltax;
    numpixels = deltay; //There are more y-values than x-values
  }
  for (curpixel = 0; curpixel <= numpixels; curpixel++)
  {
    pset(x % w, y % h, color);  //Draw the current pixel
    num += numadd;  //Increase the numerator by the top of the fraction
    if (num >= den) //Check if numerator >= denominator
    {
      num -= den; //Calculate the new numerator value
      x += xinc1; //Change the x as appropriate
      y += yinc1; //Change the y as appropriate
    }
    x += xinc2; //Change the x as appropriate
    y += yinc2; //Change the y as appropriate
  }

  return 1;
}

//Bresenham circle with center at (xc,yc) with radius and red green blue color
bool drawCircle(int xc, int yc, int radius, j_color color)
{
  if(xc - radius < 0 || xc + radius >= w || yc - radius < 0 || yc + radius >= h) return 0;
  int x = 0;
  int y = radius;
  int p = 3 - (radius << 1);
  int a, b, c, d, e, f, g, h;
  while (x <= y)
  {
     a = xc + x; //8 pixels can be calculated at once thanks to the symmetry
     b = yc + y;
     c = xc - x;
     d = yc - y;
     e = xc + y;
     f = yc + x;
     g = xc - y;
     h = yc - x;
     pset(a, b, color);
     pset(c, d, color);
     pset(e, f, color);
     pset(g, f, color);
     if(x > 0) //avoid drawing pixels at same position as the other ones
     {
       pset(a, d, color);
       pset(c, b, color);
       pset(e, h, color);
       pset(g, h, color);
     }
     if(p < 0) p += (x++ << 2) + 6;
     else p += ((x++ - y--) << 2) + 10;
  }

  return 1;
}

//Filled bresenham circle with center at (xc,yc) with radius and red green blue color
bool drawDisk(int xc, int yc, int radius, j_color color)
{
  if(xc + radius < 0 || xc - radius >= w || yc + radius < 0 || yc - radius >= h) return 0; //every single pixel outside screen, so don't waste time on it
  int x = 0;
  int y = radius;
  int p = 3 - (radius << 1);
  int a, b, c, d, e, f, g, h;
  int pb = yc + radius + 1, pd = yc + radius + 1; //previous values: to avoid drawing horizontal lines multiple times  (ensure initial value is outside the range)
  while (x <= y)
  {
     // write data
     a = xc + x;
     b = yc + y;
     c = xc - x;
     d = yc - y;
     e = xc + y;
     f = yc + x;
     g = xc - y;
     h = yc - x;
     if(b != pb) horLine(b, a, c, color);
     if(d != pd) horLine(d, a, c, color);
     if(f != b)  horLine(f, e, g, color);
     if(h != d && h != f) horLine(h, e, g, color);
     pb = b;
     pd = d;
     if(p < 0) p += (x++ << 2) + 6;
     else p += ((x++ - y--) << 2) + 10;
  }

  return 1;
}

//Fast horizontal line from (x1,y) to (x2,y), with rgb color
bool horLine(int y, int x1, int x2, j_color color)
{
  if(x2 < x1) {x1 += x2; x2 = x1 - x2; x1 -= x2;} //swap x1 and x2, x1 must be the leftmost endpoint
  if(x2 < 0 || x1 >= w || y < 0 || y >= h) return 0; //no single point of the line is on screen
  if(x1 < 0) x1 = 0; //clip
  if(x2 >= w) x2 = w - 1; //clip

  Uint32 colorSDL = SDL_MapRGB(window_surface->format, color.r, color.g, color.b);
  Uint32* bufp;
  bufp = (Uint32*) window_surface->pixels + y * window_surface->pitch / 4 + x1;
  for(int x = x1; x <= x2; x++)
  {
    *bufp = colorSDL;
    bufp++;
  }
  return 1;
}

//Puts an RGB color pixel at position x,y
void pset(int x, int y, j_color color)
{
  if(x < 0 || y < 0 || x >= w || y >= h) return;
  Uint32 colorSDL = SDL_MapRGB(window_surface->format, color.r, color.g, color.b);
  Uint32* bufp;
  bufp = (Uint32*) window_surface->pixels + y * window_surface->pitch / 4 + x;
  *bufp = colorSDL;
}


