#ifndef __RENDER_H__
#define __RENDER_H__
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <random>
#include <chrono>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#define J_PI 3.14159265358979323846264338327950288
#define J_2PI 3.14159265358979323846264338327950288 * 2
#define J_3PI 3.14159265358979323846264338327950288 * 3
#define J_4PI 3.14159265358979323846264338327950288 * 4

extern int mouseX;
extern int mouseY;

extern int h;
extern int w;

extern SDL_Window * sdlWindow;
extern SDL_Renderer * sdlRenderer;
extern SDL_Surface *sdlScreen;
extern SDL_Texture * sdlTexture;

typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} j_color;

const j_color Colors[49] = {
    {0x21, 0x96, 0xF3},{0x1E, 0x88, 0xE5},{0x19, 0x76, 0xD2},{0x15, 0x65, 0xC0},
    {0x0D, 0x47, 0xA1},{0x82, 0xB1, 0xFF},{0x44, 0x8A, 0xFF},{0x29, 0x79, 0xFF},
    {0x29, 0x62, 0xFF},{0x55, 0x25, 0x86},{0x6A, 0x35, 0x9C},{0x80, 0x4F, 0xB3},
    {0x99, 0x69, 0xC7},{0xB5, 0x89, 0xD6},{0xFA, 0x80, 0x72},{0xB2, 0x22, 0x22},
    {0x8B, 0x00, 0x00},{0xB8, 0x0F, 0x0A},{0xFF, 0x28, 0x00},{0xBF, 0x0A, 0x30},
    {0x96, 0x00, 0x19},{0x71, 0x0C, 0x04},{0x00, 0x00, 0x00},{0xFF, 0x00, 0x00},
    {0x00, 0x80, 0x00},{0x98, 0xFB, 0x98},{0x2E, 0x8B, 0x57},{0x00, 0xFF, 0xFF},
    {0xFF, 0x00, 0xFF},{0xFF, 0xFF, 0x00},{0xFF, 0xFF, 0xFF},{0x80, 0x80, 0x80},
    {0xC0, 0xC0, 0xC0},{0x80, 0x00, 0x00},{0x00, 0x8F, 0x00},{0x00, 0x00, 0x80},
    {0x00, 0x80, 0x80},{0x80, 0x80, 0x00},{0x6B, 0x8E, 0x23},{0x55, 0x6B, 0x2F},
    {0x00, 0xFF, 0x00},{0xF9, 0x0F, 0xC0},{0xFF, 0xA5, 0x00},{0xFF, 0x8C, 0x00},
    {0xFF, 0x45, 0x00},{0xFF, 0x63, 0x47},{0xF9, 0xA6, 0x02},{0xE0, 0x11, 0x5F},
    {0xFC, 0xA3, 0xB7}
};

enum{J_Blue,J_Darkblue,J_Darkblue2,J_Darkblue3,J_Darkblue4,
     J_Blueaccent,J_Blueaccent2,J_Blueaccent3,J_Blueaccent4,
     J_Purple,J_Purple2,J_Purple3,J_Purple4,J_Purple5,
     J_Salmon,J_Firebrick,J_Darkred,J_Crimson,J_Ferrari,J_USFlag,J_Carmine,J_Blood,
     J_Black,J_Red,J_Green,J_Greenpale,J_Greensea,J_Cyan,J_Magenta,
     J_Yellow,J_White,J_Gray,J_Grey,J_Maroon,J_Darkgreen,J_Navy,J_Teal,
     J_Olive,J_Olivedrab,J_Olivedark,J_Lime,J_Pink,J_Orange,
     J_Orangedark,J_Oranger,J_Tomato,J_Gold,J_Ruby,J_Flamingo,}ColorNames;

void createRenderer(int width, int height, bool fullscreen);
void pset(int x, int y, j_color color);
bool done(bool quit_if_esc, bool delay);
void readKeys();
int jrandom(int max_);
int jrandom(int min_, int max_);
double jrandomR();
bool drawCircle(int xc, int yc, int radius, j_color color);
double distance(double x_, double y_, double x_2, double y_2);
double constrain(double value,double low, double high);
int j_abs(int a);
bool horLine(int y, int x1, int x2, j_color color);
bool drawDisk(int xc, int yc, int radius, j_color color);
void cls(j_color color_);
double map(double value,double start1, double stop1, double start2, double stop2, bool withinBounds);
bool drawRect(int x1, int y1, int x2, int y2, j_color color);
#endif