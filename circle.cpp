#include "circle.hpp"
#include "renderer.hpp"

void j_circle::grow(double s_) {
  if(growing) {
      r += s_;
  }
}

bool j_circle::edges() {
    return (x + r > w || x -  r < 0 || y + r > h || y -r < 0); 
}

void j_circle::show() {
    drawCircle(x,y,r,Colors[J_White]);
}

