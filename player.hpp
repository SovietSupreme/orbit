#ifndef _PLAYER_HPP_
#define _PLAYER_HPP_
#include <stdio.h>
#include <stdlib.h>

#include "vector.hpp"
#include "renderer.hpp"

class player {
    private:
        j_vector2d * pos;
        j_color color;
        int height;
        int width;
    public:
        player(int _x, int _y, int _h, int _w, j_color _color) {
            pos = new j_vector2d(_x,_y);
            height = _h;
            width = _w;
            color = _color;
        }
        ~player() {
            delete(pos);
         }
        void update();
	    void show();

};



#endif