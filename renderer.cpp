#include <math.h>
#include "renderer.hpp"

int mouseX;
int mouseY;
int h;
int w;

unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

SDL_Window * sdlWindow;
SDL_Renderer * sdlRenderer;
SDL_Surface *sdlScreen;
SDL_Texture * sdlTexture;
SDL_Event event = {0};
const uint8_t* inkeys;
std::mt19937 generator (seed);

void createRenderer(int width, int height, bool fullscreen) {
    printf("seed: %i\n",seed);
    w = width;
    h = height;

    if(fullscreen) {
        sdlWindow = SDL_CreateWindow("SDL2",
                             SDL_WINDOWPOS_UNDEFINED,
                             SDL_WINDOWPOS_UNDEFINED,
                             width, height,
                             SDL_WINDOW_FULLSCREEN_DESKTOP);
    } else {
        sdlWindow = SDL_CreateWindow("SDL2",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             width, height,
                             0);
    }

    if(sdlWindow == NULL) {
            printf("Failed to create sdlWindow!!\n\n");
            exit(-1);
    }

    sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, SDL_RENDERER_ACCELERATED);

    if(sdlRenderer == NULL) {
            printf("Failed to create sdlRenderer!!\n\n");
            exit(-1);
    }

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");  // make the scaled rendering look smoother.
    SDL_RenderSetLogicalSize(sdlRenderer, width, height);

    sdlScreen = SDL_CreateRGBSurface(0, width, height, 32,
                                        0x00FF0000,
                                        0x0000FF00,
                                        0x000000FF,
                                        0xFF000000);

    if(sdlScreen == NULL) {
            printf("Failed to create sdlScreen!!\n\n");
            exit(-1);
    }

    sdlTexture = SDL_CreateTextureFromSurface(sdlRenderer, sdlScreen);

    if(sdlTexture == NULL) {
            printf("Failed to create sdlTexture!!\n\n");
            exit(-1);
    }

    SDL_SetRenderDrawColor( sdlRenderer, 0x00, 0x00, 0x00, 0xFF );

}

bool done(bool quit_if_esc, bool delay) 
{
  if(delay) SDL_Delay(5);
  
  bool doneFlag = false;

  if(!SDL_PollEvent(&event)) {
    return doneFlag;
  }
  
  readKeys();
  
  SDL_GetMouseState(&mouseX,&mouseY);

  if(quit_if_esc && inkeys[SDL_SCANCODE_ESCAPE]) {
    doneFlag = true;
  }

  if(inkeys[SDL_SCANCODE_SPACE]) {
    cls(Colors[J_Black]);
  }
  
  if(event.type == SDL_QUIT) { 
    doneFlag = true;
  }
  return doneFlag;
}

void readKeys()
{
  SDL_PollEvent(&event);
  inkeys = SDL_GetKeyboardState(NULL);
}

double map(double value,double start1, double stop1, double start2, double stop2, bool withinBounds) {
  
  double newval = (value - start1) / (stop1 - start1) * (stop2 - start2) + start2;
  
  if(!withinBounds) {
    return newval;
  }
  if (start2 < stop2) {
    return constrain(newval, start2, stop2);
  } else {
    return constrain(newval, stop2, start2);
  }
}

int jrandom(int max_) {
  std::uniform_int_distribution<int> distribution(0,max_);
  return distribution(generator);
}

int jrandom(int min_, int max_) {
  std::uniform_int_distribution<int> distribution(min_,max_);
  return distribution(generator);
}

double jrandomR() {
  std::uniform_real_distribution<double> distribution(0.0000,1.0000);
  return distribution(generator);
}

int j_abs(int a)
{
	return (a < 0) ? -a : a;
}

double constrain(double value,double low, double high) {
    return MAX(MIN(value,high),low);
}


double distance(double x_, double y_, double x_2, double y_2) {
  return hypot(x_2 - x_, y_2 - y_);
}

void cls(j_color color_) {
    SDL_FillRect( sdlScreen, NULL, SDL_MapRGB( sdlScreen->format, color_.r, color_.g, color_.b ) );
}


//Puts an RGB color pixel at position x,y
void pset(int x, int y, j_color color)
{
  if(x < 0 || y < 0 || x >= w || y >= h) return;
  Uint32 colorSDL = SDL_MapRGB(sdlScreen->format, color.r, color.g, color.b);
  Uint32* bufp;
  bufp = (Uint32*) sdlScreen->pixels + y * sdlScreen->pitch / 4 + x;
  *bufp = colorSDL;
}

//Bresenham circle with center at (xc,yc) with radius and red green blue color
bool drawCircle(int xc, int yc, int radius, j_color color)
{
  if(xc - radius < 0 || xc + radius >= w || yc - radius < 0 || yc + radius >= h) return 0;
  int x = 0;
  int y = radius;
  int p = 3 - (radius << 1);
  int a, b, c, d, e, f, g, h;
  while (x <= y)
  {
     a = xc + x; //8 pixels can be calculated at once thanks to the symmetry
     b = yc + y;
     c = xc - x;
     d = yc - y;
     e = xc + y;
     f = yc + x;
     g = xc - y;
     h = yc - x;
     pset(a, b, color);
     pset(c, d, color);
     pset(e, f, color);
     pset(g, f, color);
     if(x > 0) //avoid drawing pixels at same position as the other ones
     {
       pset(a, d, color);
       pset(c, b, color);
       pset(e, h, color);
       pset(g, h, color);
     }
     if(p < 0) p += (x++ << 2) + 6;
     else p += ((x++ - y--) << 2) + 10;
  }

  return 1;
}

//Filled bresenham circle with center at (xc,yc) with radius and red green blue color
bool drawDisk(int xc, int yc, int radius, j_color color)
{
  if(xc + radius < 0 || xc - radius >= w || yc + radius < 0 || yc - radius >= h) return 0; //every single pixel outside screen, so don't waste time on it
  int x = 0;
  int y = radius;
  int p = 3 - (radius << 1);
  int a, b, c, d, e, f, g, h;
  int pb = yc + radius + 1, pd = yc + radius + 1; //previous values: to avoid drawing horizontal lines multiple times  (ensure initial value is outside the range)
  while (x <= y)
  {
     // write data
     a = xc + x;
     b = yc + y;
     c = xc - x;
     d = yc - y;
     e = xc + y;
     f = yc + x;
     g = xc - y;
     h = yc - x;
     if(b != pb) horLine(b, a, c, color);
     if(d != pd) horLine(d, a, c, color);
     if(f != b)  horLine(f, e, g, color);
     if(h != d && h != f) horLine(h, e, g, color);
     pb = b;
     pd = d;
     if(p < 0) p += (x++ << 2) + 6;
     else p += ((x++ - y--) << 2) + 10;
  }

  return 1;
}

//Fast horizontal line from (x1,y) to (x2,y), with rgb color
bool horLine(int y, int x1, int x2, j_color color)
{
  if(x2 < x1) {x1 += x2; x2 = x1 - x2; x1 -= x2;} //swap x1 and x2, x1 must be the leftmost endpoint
  if(x2 < 0 || x1 >= w || y < 0 || y >= h) return 0; //no single point of the line is on screen
  if(x1 < 0) x1 = 0; //clip
  if(x2 >= w) x2 = w - 1; //clip

  Uint32 colorSDL = SDL_MapRGB(sdlScreen->format, color.r, color.g, color.b);
  Uint32* bufp;
  bufp = (Uint32*) sdlScreen->pixels + y * sdlScreen->pitch / 4 + x1;
  for(int x = x1; x <= x2; x++)
  {
    *bufp = colorSDL;
    bufp++;
  }
  return 1;
}

//Rectangle with corners (x1,y1) and (x2,y2) and rgb color
bool drawRect(int x1, int y1, int x2, int y2, j_color color)
{
  if(x1 < 0 || x1 > w - 1 || x2 < 0 || x2 > w - 1 || y1 < 0 || y1 > h - 1 || y2 < 0 || y2 > h - 1) return 0;
  SDL_Rect rec;
  rec.x = x1;
  rec.y = y1;
  rec.w = x2 - x1 + 1;
  rec.h = y2 - y1 + 1;
  Uint32 colorSDL = SDL_MapRGB(sdlScreen->format, color.r, color.g, color.b);
  SDL_FillRect(sdlScreen, &rec, colorSDL);  //SDL's ability to draw a hardware rectangle is used for now
  return 1;
}