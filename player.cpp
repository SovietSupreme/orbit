#ifndef _PLAYER_CPP_
#define _PLAYER_CPP_

#include "player.hpp"

void player::update() {
    pos->x = mouseX;
    pos->y = mouseY;
}

void player::show() {
    drawRect(pos->x,pos->y,pos->x + width, pos->y + height, color);
}

#endif