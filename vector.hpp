#ifndef __VECTOR_H__
#define __VECTOR_H__
#include <stdio.h>
#include <math.h>

#define DEG_TO_RAD 3.14159265358979323846264338327950288 / 180.0
#define RAD_TO_DEG 180.0 / 3.14159265358979323846264338327950288

class j_vector2d {
    private:
       j_vector2d * fromAngle(double angle_, int length_);
    public:
        //
        // coords
        //
        double x = 0.000;
        double y = 0.000;
        j_vector2d() {

        }
        //
        // Create from x,y coords
        //
        j_vector2d(double x_, double y_) {
            x = x_;
            y = y_;
        }
        ~j_vector2d() {
            printf("vector object destroyed!!\n");
        }
    
        void normalize();
        double radians(double angle);
        double magSq();
        double mag();
        void random2d();
        double heading();
        void rotate(double deg);

};


#endif