#ifndef _BOUNDARY_H
#define _BOUNDARY_H 
#include "vector.hpp"

class j_boundary {
    public:
        j_vector2d *a;
        j_vector2d *b;
    
        j_boundary(int x1, int y1, int x2, int y2) {
             a = new j_vector2d(x1,y1);
             b = new j_vector2d(x2,y2);
        }

};
#endif