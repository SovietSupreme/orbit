#include "ray.h"

j_ray createRay(j_vector2d pos, double angle) {
    j_ray newRay;
    newRay.pos = pos;
    newRay.dir = fromAngle(angle);
    //newRay.lookAt = lookAt;
    return newRay;
}

void lookAt(j_ray* ray, double x, double y) {
    ray->dir.x = x - ray->pos.x;
    ray->dir.y = y - ray->pos.y;
    j_vector2d* direction = &ray->dir;
    normalize(direction);
}