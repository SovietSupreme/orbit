#include <math.h>
#include "vector.hpp"
#include "renderer.hpp"

double j_vector2d::radians(double angle_) {
    return angle_ * DEG_TO_RAD;
}

double j_vector2d::magSq() {
    return x * x + y * y;
}

double j_vector2d::mag() {
    return sqrt(magSq());
}

void j_vector2d::normalize() {
    double len = mag();

    if(len != 0) {
        x = x * (1 / len);
        y = y * (1 / len);
    }
}

 j_vector2d * j_vector2d::fromAngle(double angle_, int length_ = 1) {
    return new j_vector2d(length_ * cos(angle_), length_ * sin(angle_));
}

 void j_vector2d::random2d() {
    j_vector2d * result = fromAngle(radians(jrandom(650000) * J_2PI));
    x = result->x;
    y = result->y;
    delete(result);
}

double j_vector2d::heading() {
    double h = atan2(y,x);
    return (h * RAD_TO_DEG);
}

void j_vector2d::rotate(double deg_) {
    double newHeading = (heading() + deg_) * (DEG_TO_RAD);
    double magnitude = mag();
    x = cos(newHeading) * magnitude;
    y = sin(newHeading) * magnitude;
}




