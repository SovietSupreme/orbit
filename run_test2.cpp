#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>

#include "renderer.hpp"
#include "vector.hpp"
#include "boundary.hpp"
#include "bmp.h"
#include "planet.hpp"
#include "circle.hpp"
#include "player.hpp"


int main(int argc, char** argv) {

    createRenderer(1500,1000,false);


    //
    // SETUP
    //
    //Planet *next = new Planet(w/2,h/2,5,w/10);
    player *p1 = new player(mouseX,mouseY,30,30,Colors[J_Blueaccent2]);

    cls(Colors[J_Black]);

   // for(int i = 0; i<6; i++){
   //    next->addChild();
   //    next = next->child;
   //    next->color = Colors[jrandom(49)];
   // }
 /*
    double theta1 = 0.000;
    double theta2 = 0.000;

    j_color sineColor = Colors[jrandom(49)];

    double gravity = 0.156;
    double speed = 0.0000;
    double ballX = w / 2;
    double ballY = h / 2;
*/

     while(!done(true,false)) {
          
        cls(Colors[J_Black]);
        p1->update();
        p1->show();
/*
          double x = (cos(theta1) + 1) * (w / 2);
          double y = (sin(theta2) + 1) * (h / 2);

          theta1 += 0.0027;
          theta2 += 0.0064;

          drawDisk(x,y,1,sineColor);
*/
    
       // while(next->parent != NULL) {
       //     next->update();
       //     next = next->parent;
       // }

       // while(next->child != NULL) {
       //     next->show();
       //     next = next->child;
       // } 


    

        SDL_UpdateTexture(sdlTexture, NULL, sdlScreen->pixels, sdlScreen->pitch);

        
        SDL_RenderClear(sdlRenderer);

        //Render texture to screen
        SDL_RenderCopy( sdlRenderer, sdlTexture, NULL, NULL );

        //Update screen
        SDL_RenderPresent( sdlRenderer );

        //SDL_FillRect( sdlScreen, NULL, SDL_MapRGB( sdlScreen->format, 0x00, 0x00, 0x00 ) );
    }

   
    //Planet * tmp;
    //while(next != NULL) {
    //  *tmp = *next->parent;
    //  delete(next);
    //  *next = *tmp->parent;
    //  delete(tmp);
   // }

      //Free loaded image
    SDL_DestroyTexture( sdlTexture );
    sdlTexture = NULL;

    //Destroy window    
    SDL_DestroyRenderer( sdlRenderer );
    SDL_DestroyWindow( sdlWindow );
    sdlWindow = NULL;
    sdlRenderer = NULL;

    //Quit SDL subsystems
    IMG_Quit();
    SDL_Quit();

    return 1;

}


