#ifndef _RAY_H_
#define _RAY_H_
#include "vector.hpp"

typedef struct j_ray {
    
    j_vector2d dir;
    j_vector2d pos;

} j_ray;

j_ray createRay(j_vector2d pos, double angle);

void lookAt(j_ray* ray, double x, double y);
#endif