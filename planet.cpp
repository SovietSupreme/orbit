
#ifndef _PLANET_CPP_
#define _PLANET_CPP_
#include "planet.hpp"
#include "renderer.hpp"

void Planet::addChild() {
    int newr = 0;
    int newx = 0;
    int newy = 0;
    newr = r / 2;
    newx = pos->x + pos->y + r;
    newy = pos->y;

    if(newr <=0 ) {
        newr = jrandom(1,2);
    }

   //printf("adding child with r of %i\n",newr);
    
    child = new Planet(newx,newy,n,newr,this);
}

void Planet::update() {
    if(parent != NULL) {
       angle += parent->speed;
       int rsum = r + parent->r;
       pos->x = parent->pos->x + rsum * cos(angle);
       pos->y = parent->pos->y + rsum * sin(angle);
    }
}

void Planet::show() {
   if(numParents() > 1) {
       if(numChildren() == 1) {
           color = Colors[J_Tomato];
           pset(pos->x,pos->y,color);
       } else {
            if(color.r == 0x00 && color.g == 0x00 && color.b == 0x00) {
                color = Colors[J_Orange];
            }    
           pset(pos->x,pos->y,color);
       } 
   } else {
       color = Colors[J_Darkblue4];
       pset(pos->x,pos->y,color);
   }
  
}

int Planet::numParents() {
    int cnt = 0;
    if(parent == NULL) {
        return cnt;
    } else {
        Planet *p_ = parent;
        while(p_ != NULL) {
            p_ = p_->parent;
            cnt++;
        }
    }
    return cnt;
}

int Planet::numChildren() {
    int cnt = 0;
    if(child == NULL) {
        return cnt;
    } else {
        Planet *c_ = child;
        while(c_ != NULL) {
            c_ = c_->child;
            cnt++;
        }
    }
    return cnt;
}


#endif