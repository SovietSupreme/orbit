#include <stdio.h>
#include "vector.hpp"

class j_circle {
    private:
        
    public:
        double x;
        double y;
        double z;
        double r;
        bool growing = true;

        j_circle(double x_, double y_) {
            x = x_;
            y = y_;
            r = 3;
        }
        ~j_circle() {
            printf("circle object destroyed!!\n");
        }
        

        void grow(double s_ = 0.5);
        bool edges();
        void show();

};